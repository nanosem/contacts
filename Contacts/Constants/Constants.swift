//
//  Constants.swift
//  Contacts
//
//  Created by  Nanosem on 24.07.17.
//  Copyright © 2017 Shintio. All rights reserved.
//

import Foundation

struct Constants {
    struct SegueIdentifiers {
        static let MainToContact = "MainToContact"
    }
}
