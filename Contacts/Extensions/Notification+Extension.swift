//
//  Notification+Extension.swift
//  Contacts
//
//  Created by  Nanosem on 24.07.17.
//  Copyright © 2017 Shintio. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let DidAddNewContact = Notification.Name(rawValue: "DidAddNewContact")
    static let DidDeleteContact = Notification.Name(rawValue: "DidDeleteContact")
    static let DidEditContact   = Notification.Name(rawValue: "DidEditContact")
}
