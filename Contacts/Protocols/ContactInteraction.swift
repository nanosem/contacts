//
//  ContactInteraction.swift
//  Contacts
//
//  Created by  Nanosem on 24.07.17.
//  Copyright © 2017 Shintio. All rights reserved.
//

import Foundation
protocol ContactInteraction {
    func add(contact: Contact)
    func edit(contact: Contact)
    func delete(contact: Contact)
}
