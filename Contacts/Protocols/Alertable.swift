//
//  Alertable.swift
//  Contacts
//
//  Created by  Nanosem on 25.07.17.
//  Copyright © 2017 Shintio. All rights reserved.
//

import UIKit

protocol Alertable {
    func showMessage(title: String?, message: String?, handler: (() -> Void)?)
}

extension Alertable where Self: UIViewController {
    func showMessage(title: String?, message: String?, handler: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel) { _ in
            handler?()
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}
