//
//  ReusableView.swift
//  Contacts
//
//  Created by  Nanosem on 24.07.17.
//  Copyright © 2017 Shintio. All rights reserved.
//

import UIKit

protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return String(describing: Self.self)
    }
}
