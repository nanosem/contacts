//
//  ContactViewCell.swift
//  Contacts
//
//  Created by  Nanosem on 24.07.17.
//  Copyright © 2017 Shintio. All rights reserved.
//

import UIKit

class ContactViewCell: UITableViewCell, NibLoadableView, ReusableView {
    
    // MARK: - Variables
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var firstNameLabel: UILabel!
    @IBOutlet private weak var secondNameLabel: UILabel!
    @IBOutlet private weak var patronymicLabel: UILabel!
    
    static let cellHeight: CGFloat = 92
    
    // MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Functions
    func setup(withPhoto image: UIImage?, firstName: String, secondName: String, patronymic: String) {
        photoImageView.image = image
        firstNameLabel.text  = firstName
        secondNameLabel.text = secondName
        patronymicLabel.text = patronymic
    }
}
