//
//  DataManager.swift
//  Contacts
//
//  Created by  Nanosem on 24.07.17.
//  Copyright © 2017 Shintio. All rights reserved.
//

import UIKit

class DataManager {
    // MARK: - Variables
    static let instance = DataManager()
    fileprivate(set) var contacts = [Character: [Contact]]() {
        didSet {
            keys = contacts.keys.sorted()
        }
    }
    private(set) var keys = [Character]()
    
    // MARK: - Functions
    private init() {}
    
    fileprivate func getKey(for contact: Contact) -> Character? {
        guard let key = contact.secondName.characters.first else { return nil }
        return key
    }
}

// MARK: - ContactInteraction
extension DataManager: ContactInteraction {
    func add(contact: Contact) {
        guard let key = getKey(for: contact) else { return }
        if var contactsForKey = contacts[key] {
            contactsForKey.append(contact)
            contactsForKey.sort(by: { $0.secondName < $1.secondName })
            contacts.updateValue(contactsForKey, forKey: key)
        } else {
            contacts[key] = [contact]
        }
        
        NotificationCenter.default.post(name: .DidAddNewContact, object: nil)
    }
    
    func edit(contact: Contact) {
        guard let key = getKey(for: contact) else { return }
        
        var oldContactOpt: Contact?
        
        for contactsForKey in contacts.values {
            for currentContact in contactsForKey where currentContact.id == contact.id {
                oldContactOpt = currentContact
            }
        }
        
        guard let oldContact = oldContactOpt,
            let oldKey = getKey(for: oldContact),
            var contactsForKey = contacts[oldKey] else { return }
        
        if oldKey == key {
            guard let index = contactsForKey.index(of: contact) else { return }
            contactsForKey[index] = contact
            contacts[key] = contactsForKey
            NotificationCenter.default.post(name: .DidEditContact, object: nil)
        } else {
            delete(contact: oldContact)
            add(contact: contact)
        }
    }
    
    func delete(contact: Contact) {
        guard let key = getKey(for: contact),
            var contactsForKey = contacts[key],
            let index = contactsForKey.index(of: contact) else { return }
        contactsForKey.remove(at: index)
        contacts[key] = contactsForKey.sorted(by: { $0.secondName < $1.secondName })
        if contactsForKey.isEmpty {
            contacts.updateValue([], forKey: key)
        }
    }
}
