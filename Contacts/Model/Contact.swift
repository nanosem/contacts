//
//  Contact.swift
//  Contacts
//
//  Created by  Nanosem on 24.07.17.
//  Copyright © 2017 Shintio. All rights reserved.
//

import UIKit

struct Contact {
    private static var counter = 0
    var id: String
    var name: String
    var secondName: String
    var patronymic: String
    var photo: UIImage?
    
    init(name: String, secondName: String, patronymic: String, photo: UIImage?) {
        self.id = "\(Contact.counter)"
        Contact.counter += 1
        
        self.name = name
        self.secondName = secondName
        self.patronymic = patronymic
        self.photo = photo
    }
}

extension Contact: Equatable {
    static func ==(lhs: Contact, rhs: Contact) -> Bool {
        if  lhs.id == rhs.id  {
            return true
        }
        return false
    }
}
