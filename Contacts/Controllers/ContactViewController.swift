//
//  ContactViewController.swift
//  Contacts
//
//  Created by  Nanosem on 24.07.17.
//  Copyright © 2017 Shintio. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController, Alertable {
    
    // MARK: - Variables
    @IBOutlet fileprivate weak var photoImageView: UIImageView!
    @IBOutlet fileprivate weak var firstNameField: UITextField!
    @IBOutlet fileprivate weak var secondNameField: UITextField!
    @IBOutlet fileprivate weak var patronymicField: UITextField!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    var contact: Contact?
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) {
            [weak self] notification in
            guard let strongSelf = self else { return }
            strongSelf.keyboardWillShow(notification: notification)
        }
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) {
            [weak self] notification in
            guard let strongSelf = self else { return }
            strongSelf.keyboardWillHide(notification: notification)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Functions
    private func setupView() {
        if let contact = contact {
            navigationItem.title = "Изменить"
            photoImageView.image = contact.photo
            firstNameField.text  = contact.name
            secondNameField.text = contact.secondName
            patronymicField.text = contact.patronymic
        } else {
            navigationItem.title = "Новый"
            photoImageView.image = #imageLiteral(resourceName: "contactStandart")
        }
    }
    
    private func keyboardWillShow(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let value = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)  else { return }
        let frame = value.cgRectValue
        let contextInset = UIEdgeInsets(top: 0, left: 0, bottom: frame.height, right: 0)
        scrollView.contentInset = contextInset
    }
    
    private func keyboardWillHide(notification: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }
    
    // MARK: - Actions
    @IBAction private func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func done(_ sender: Any) {
        guard let name = firstNameField.text,
            !name.isEmpty,
            let secondName = secondNameField.text,
            !secondName.isEmpty,
            let patronymic = patronymicField.text,
            !patronymic.isEmpty,
            let photo = photoImageView.image else {
                showMessage(title: nil, message: "Заполните все поля!", handler: nil)
                return
            }
        
        if var contact = contact {
            contact.name = name
            contact.secondName = secondName
            contact.patronymic = patronymic
            contact.photo = photo
            DataManager.instance.edit(contact: contact)
        } else {
            let contact = Contact(name: name,
                                  secondName: secondName,
                                  patronymic: patronymic,
                                  photo: photo)
            
            DataManager.instance.add(contact: contact)
        }
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Selectors
    @IBAction private func didTapView(_ sender: Any) {
        view.endEditing(true)
    }
}

extension ContactViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
        photoImageView.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func selectImage(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: nil, message: "Выберите источник", preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Камера", style: .default, handler: { [weak self] _ in
            guard let strongSelf = self else { return }
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                strongSelf.present(imagePicker, animated:  true, completion: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { [weak self] _ in
            guard let strongSelf = self else { return }
            imagePicker.sourceType = .photoLibrary
            strongSelf.present(imagePicker, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
}

// MARK: - UITextFieldDelegate
extension ContactViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag != 3 {
            view.viewWithTag(textField.tag + 1)?.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
}
