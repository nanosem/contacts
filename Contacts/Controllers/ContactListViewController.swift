//
//  ContactListViewController.swift
//  Contacts
//
//  Created by  Nanosem on 24.07.17.
//  Copyright © 2017 Shintio. All rights reserved.
//

import UIKit

class ContactListViewController: UITableViewController {
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ContactViewCell.self)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadView),
                                               name: .DidAddNewContact,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadView),
                                               name: .DidEditContact,
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - TableView DataSource & Delegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        return DataManager.instance.keys.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ContactViewCell.cellHeight
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let contactsForKey = getContacts(at: section) else { return 0 }
        return contactsForKey.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let contactsForKey = getContacts(at: section) else { return nil }
        if !contactsForKey.isEmpty {
            return "\(getKey(for: section))"
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ContactViewCell = tableView.dequeueReusableCell(for: indexPath)
        
        guard let contact = getContact(for: indexPath) else { return cell }
        
        cell.setup(withPhoto: contact.photo,
                   firstName: contact.name,
                   secondName: contact.secondName,
                   patronymic: contact.patronymic)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let contact = getContact(for: indexPath)
        performSegue(withIdentifier: Constants.SegueIdentifiers.MainToContact, sender: contact)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            guard let contact = getContact(for: indexPath) else { return }
            DataManager.instance.delete(contact: contact)
            tableView.deleteRows(at: [indexPath], with: .fade)
            reloadView()
        }
    }
    
    // MARK: - Prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.SegueIdentifiers.MainToContact {
            guard let destination = segue.destination as? ContactViewController else { return }
            destination.contact = sender as? Contact
        }
    }
    
    // MARK: - Private functions
    @IBAction private func addContact(_ sender: Any) {
        performSegue(withIdentifier: Constants.SegueIdentifiers.MainToContact, sender: nil)
    }
    
    private func getContact(for indexPath: IndexPath) -> Contact? {
        guard let contactsForKey = getContacts(at: indexPath.section) else { return nil }
        return contactsForKey[indexPath.row]
    }
    
    private func getContacts(at section: Int) -> [Contact]? {
        let key = getKey(for: section)
        guard let contactsForKey = DataManager.instance.contacts[key] else { return nil }
        return contactsForKey
    }
    
    private func getKey(for section: Int) -> Character {
        let keys = DataManager.instance.keys
        let key = keys[section]
        return key
    }
    
    // MARK: - Selectors
    @objc private func reloadView() {
        tableView.reloadData()
    }
}
